package com.simpleappsgroup.tasks3;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.simpleappsgroup.tasks3.data.TasksContentProvider;
import com.simpleappsgroup.tasks3.data.TasksContract;
import com.simpleappsgroup.tasks3.data.TasksDbHelper;

public class AddNewTaskDialogFragment extends DialogFragment {

    private OnBacklogItemAddedListener callback;

    public static AddNewTaskDialogFragment newInstance(OnBacklogItemAddedListener callback) {
        AddNewTaskDialogFragment fragment = new AddNewTaskDialogFragment();
        fragment.callback = callback;
        return fragment;
    }

    public interface OnBacklogItemAddedListener {
        void onBacklogItemAdded();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.add_task_dialog, null);
        builder.setView(dialogView)
                .setTitle(R.string.add_new_item)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText backlogItemDesc = (EditText) dialogView.findViewById(R.id.task_desc);
                        String backlogItemDescText = backlogItemDesc.getText().toString();
                        createNewBacklogItem(backlogItemDescText);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AddNewTaskDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    private void createNewBacklogItem(String backlogItemDescText) {
        ContentValues values = new ContentValues();
        values.put(TasksContract.TaskEntry.TASK_DESC_COLUMN, backlogItemDescText);
        values.put(TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN, TasksDbHelper.TRUE);
        getActivity().getContentResolver().insert(TasksContentProvider.TASKS_URI, values);

        if (callback != null) {
            callback.onBacklogItemAdded();
        }
    }
}
