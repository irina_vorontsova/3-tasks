package com.simpleappsgroup.tasks3;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.daimajia.swipe.util.Attributes;
import com.simpleappsgroup.tasks3.data.TasksDbHelper;

import static com.simpleappsgroup.tasks3.data.TaskCursorHelper.TASK_COLUMNS;
import static com.simpleappsgroup.tasks3.data.TasksContentProvider.TASKS_URI;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_DONE_COLUMN;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN;

public class TasksFragment extends Fragment implements View.OnClickListener, AddNewTaskDialogFragment.OnBacklogItemAddedListener, LoaderManager.LoaderCallbacks<Cursor> {

    public static final String ADD_BACKLOG_ITEM_DIALOG_TAG = "add_backlog_item_dialog_tag";
    public static final String SHOW_BACKLOG_ITEMS_PARAM = "showBacklogItems";

    public static final int LOADER_ID = 1;
    public static final int MAX_PLANNED_TASKS = 3;

    private boolean showBacklogItems = true;

    private Button addBacklogItemButton;
    private ListViewCursorSwipeAdapter adapter;

    public static TasksFragment newInstance(boolean showBacklogItems) {
        TasksFragment fragment = new TasksFragment();
        Bundle args = new Bundle();
        args.putBoolean(SHOW_BACKLOG_ITEMS_PARAM, showBacklogItems);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_items_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_clear_done) {
            clearDone(item);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tasks_fragment, container, false);

        Bundle args = getArguments();
        showBacklogItems = args.getBoolean(SHOW_BACKLOG_ITEMS_PARAM, true);

        addBacklogItemButton = (Button) rootView.findViewById(R.id.add_backlog_item_button);
        if (showBacklogItems) {
            addBacklogItemButton.setOnClickListener(this);
        } else {
            addBacklogItemButton.setVisibility(View.GONE);
        }
        adapter = new ListViewCursorSwipeAdapter(getActivity(), null, false);
        ListView listView = (ListView) rootView.findViewById(R.id.backlog_items_listview);
        listView.setAdapter(adapter);

        adapter.setMode(Attributes.Mode.Single);

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(LOADER_ID, null, this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = AddNewTaskDialogFragment.newInstance(this);
        newFragment.show(getFragmentManager(), ADD_BACKLOG_ITEM_DIALOG_TAG);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String whereClause = TASK_IN_BACKLOG_COLUMN + " = ";
        if (showBacklogItems) {
            whereClause += TasksDbHelper.TRUE;
        } else {
            whereClause += TasksDbHelper.FALSE;
        }
        Loader<Cursor> loader = new CursorLoader(getActivity(), TASKS_URI, TASK_COLUMNS, whereClause, null, null);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case LOADER_ID:
                adapter.swapCursor(data);
                break;
        }
    }

    public void clearDone(MenuItem item) {
        String where = TASK_DONE_COLUMN + " = " + TasksDbHelper.TRUE + " AND " + TASK_IN_BACKLOG_COLUMN + " = ";
        if (showBacklogItems) {
            where += TasksDbHelper.TRUE;
        } else {
            where += TasksDbHelper.FALSE;
        }
        int deletedRows = getActivity().getContentResolver().delete(TASKS_URI, where, null);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onBacklogItemAdded() {
        getLoaderManager().restartLoader(LOADER_ID, null, this);
    }
}
