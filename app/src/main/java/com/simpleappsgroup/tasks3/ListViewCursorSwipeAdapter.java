package com.simpleappsgroup.tasks3;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.CursorSwipeAdapter;
import com.simpleappsgroup.tasks3.data.Task;
import com.simpleappsgroup.tasks3.data.TaskCursorHelper;
import com.simpleappsgroup.tasks3.listeners.DeleteTaskListener;
import com.simpleappsgroup.tasks3.listeners.PlanForTodayTaskListener;
import com.simpleappsgroup.tasks3.listeners.TaskDoneListener;

public class ListViewCursorSwipeAdapter extends CursorSwipeAdapter {

    protected ListViewCursorSwipeAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public int getSwipeLayoutResourceId(int i) {
        return R.id.swipe_layout_id;
    }

    @Override
    public View newView(final Context context, final Cursor cursor, ViewGroup parent) {
        View swipeLayout = LayoutInflater.from(mContext).inflate(R.layout.swipe_list_item, null);
        return swipeLayout;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final Task task = TaskCursorHelper.cursorToTask(cursor);

        TextView itemDescTextView = (TextView) view.findViewById(R.id.task_desc);
        itemDescTextView.setText(task.getDescription());

        CheckBox doneCheckBox = (CheckBox) view.findViewById(R.id.in_backlog);
        doneCheckBox.setChecked(task.isDone());
        doneCheckBox.setOnCheckedChangeListener(new TaskDoneListener(task, context));

        configureSwipes(context, task, view, cursor);
    }

    private void configureSwipes(final Context context, final Task task, View swipeView, final Cursor cursor) {
        final SwipeLayout swipeLayout = (SwipeLayout) swipeView.findViewById(getSwipeLayoutResourceId(1));
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        swipeLayout.addDrag(SwipeLayout.DragEdge.Right, swipeView.findViewById(R.id.bottom_delete_layout));
        if (task.isInBacklog()) {
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeView.findViewById(R.id.bottom_move_layout));
        }

        swipeView.findViewById(R.id.delete_button).setOnClickListener(new DeleteTaskListener(task, context));
        swipeView.findViewById(R.id.move_button).setOnClickListener(new PlanForTodayTaskListener(task, swipeLayout, context));
    }

    @Override
    public void closeAllItems() {

    }
}
