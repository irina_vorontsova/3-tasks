package com.simpleappsgroup.tasks3.listeners;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.util.Log;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;
import com.simpleappsgroup.tasks3.R;
import com.simpleappsgroup.tasks3.TasksFragment;
import com.simpleappsgroup.tasks3.data.Task;
import com.simpleappsgroup.tasks3.data.TasksDbHelper;

import static android.provider.BaseColumns._ID;
import static com.simpleappsgroup.tasks3.TasksFragment.MAX_PLANNED_TASKS;
import static com.simpleappsgroup.tasks3.data.TasksContentProvider.TASKS_URI;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN;

public class PlanForTodayTaskListener implements View.OnClickListener {

    public static final String TAG = PlanForTodayTaskListener.class.getSimpleName();

    private Task task;
    private Context context;
    private SwipeLayout swipeLayout;

    public PlanForTodayTaskListener(Task task, SwipeLayout swipeLayout, Context context) {
        this.task = task;
        this.swipeLayout = swipeLayout;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        if (task != null) {
            if (getCountOfPlannedTasks(context) >= MAX_PLANNED_TASKS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context)
                        .setMessage(R.string.already_planned_3_tasks)
                        .setPositiveButton(R.string.ok_positive, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                swipeLayout.close();
                            }
                        });
                builder.show();
            } else {
                ContentValues values = new ContentValues();
                values.put(TASK_IN_BACKLOG_COLUMN, false);
                String where = _ID + "=" + task.getId();
                int updatedRows = context.getContentResolver().update(TASKS_URI, values, where, null);
                if (updatedRows > 0) {
                    Log.d(TAG, "Updated task with id " + task.getId());
                } else {
                    Log.d(TAG, "Tried to move task with id " + task.getId() + ", but failed :(");
                }
                swipeLayout.close();
            }
        }
    }

    private int getCountOfPlannedTasks(Context context) {
        Cursor countCursor = context.getContentResolver().query(TASKS_URI,
                new String[]{"count(*) AS count"},
                TASK_IN_BACKLOG_COLUMN + " = " + TasksDbHelper.FALSE,
                null,
                null);

        countCursor.moveToFirst();
        int count = countCursor.getInt(0);
        return count;
    }

}
