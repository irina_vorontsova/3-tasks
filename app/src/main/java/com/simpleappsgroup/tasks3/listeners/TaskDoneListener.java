package com.simpleappsgroup.tasks3.listeners;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.util.Log;
import android.widget.CompoundButton;

import com.simpleappsgroup.tasks3.R;
import com.simpleappsgroup.tasks3.TasksFragment;
import com.simpleappsgroup.tasks3.data.Task;
import com.simpleappsgroup.tasks3.data.TasksDbHelper;

import static android.provider.BaseColumns._ID;
import static com.simpleappsgroup.tasks3.data.TasksContentProvider.TASKS_URI;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_DONE_COLUMN;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN;

public class TaskDoneListener implements CompoundButton.OnCheckedChangeListener {

    public static final String TAG = TaskDoneListener.class.getSimpleName();

    private Task task;
    private Context context;

    public TaskDoneListener(Task task, Context context) {
        this.task = task;
        this.context = context;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (task != null) {
            ContentValues values = new ContentValues();
            int isDoneInt = isChecked ? TasksDbHelper.TRUE : TasksDbHelper.FALSE;
            values.put(TASK_DONE_COLUMN, isDoneInt);
            String where = _ID + "=" + task.getId();
            int updatedRows = context.getContentResolver().update(TASKS_URI, values, where, null);
            if (updatedRows > 0) {
                Log.d(TAG, "Done task with id " + task.getId());

                if (!task.isInBacklog() && getCountOfPlannedAndDoneTasks(context) == TasksFragment.MAX_PLANNED_TASKS) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context)
                            .setTitle(R.string.done_for_today)
                            .setMessage(R.string.get_rest)
                            .setPositiveButton(R.string.ok_positive, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                }
            } else {
                Log.d(TAG, "Tried to done task with id " + task.getId() + ", but failed :(");
            }
        }
    }

    private int getCountOfPlannedAndDoneTasks(Context context) {
        Cursor countCursor = context.getContentResolver().query(TASKS_URI,
                new String[]{"count(*) AS count"},
                TASK_IN_BACKLOG_COLUMN + " = " + TasksDbHelper.FALSE + " AND " + TASK_DONE_COLUMN + " = " + TasksDbHelper.TRUE,
                null,
                null);

        countCursor.moveToFirst();
        int count = countCursor.getInt(0);
        return count;
    }
}
