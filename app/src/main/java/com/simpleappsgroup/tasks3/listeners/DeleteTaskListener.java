package com.simpleappsgroup.tasks3.listeners;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.simpleappsgroup.tasks3.data.Task;

import static android.provider.BaseColumns._ID;
import static com.simpleappsgroup.tasks3.data.TasksContentProvider.TASKS_URI;

public class DeleteTaskListener implements View.OnClickListener {

    public static final String TAG = DeleteTaskListener.class.getSimpleName();

    private Task task;
    private Context context;

    public DeleteTaskListener(Task task, Context context) {
        this.task = task;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        if (task != null) {
            String where = _ID + " = " + task.getId();
            int deletedRows = context.getContentResolver().delete(TASKS_URI, where, null);
            if (deletedRows > 0) {
                Log.d(TAG, "Deleted task with id " + task.getId());
            } else {
                Log.d(TAG, "Tried to delete task with id " + task.getId() + ", but failed :(");
            }
        }
    }
}
