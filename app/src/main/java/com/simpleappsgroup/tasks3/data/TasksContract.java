package com.simpleappsgroup.tasks3.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class TasksContract {

    public static final String AUTHORITY = "com.simpleappsgroup.tasks3.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_TASKS = "tasks";

    public static final class TaskEntry implements BaseColumns {

        public static final String TABLE_NAME = "tasks";

        public static final String TASK_DESC_COLUMN = "description";
        public static final String TASK_IN_BACKLOG_COLUMN = "in_backlog";
        public static final String TASK_DONE_COLUMN = "done";

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + "/" + PATH_TASKS;

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TASKS).build();

    }
}
