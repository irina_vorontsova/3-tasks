package com.simpleappsgroup.tasks3.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TasksDbHelper extends SQLiteOpenHelper {

    public static final int TRUE = 1;
    public static final int FALSE = 0;

    private static final String DATABASE_NAME = "tasks.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TasksContract.TaskEntry.TABLE_NAME + " (" +
                    TasksContract.TaskEntry._ID + " INTEGER primary key autoincrement, " +
                    TasksContract.TaskEntry.TASK_DESC_COLUMN + " TEXT, " +
                    TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN + " INTEGER, " +
                    TasksContract.TaskEntry.TASK_DONE_COLUMN + " INTEGER);";

    public TasksDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TasksDbHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TasksContract.TaskEntry.TABLE_NAME);
        onCreate(db);
    }
}
