package com.simpleappsgroup.tasks3.data;

import android.database.Cursor;

public class TaskCursorHelper {

    public static final String[] TASK_COLUMNS = {
            TasksContract.TaskEntry.TABLE_NAME + "." + TasksContract.TaskEntry._ID,
            TasksContract.TaskEntry.TASK_DESC_COLUMN,
            TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN,
            TasksContract.TaskEntry.TASK_DONE_COLUMN,
    };

    // These indices are tied to TASK_COLUMNS. If TASK_COLUMNS changes, these must change.
    public static final int COL_TASK_ID = 0;
    public static final int COL_TASK_DESC = 1;
    public static final int COL_TASK_IS_IN_BACKLOG = 2;
    public static final int COL_TASK_IS_DONE = 3;

    public static Task cursorToTask(Cursor cursor) {
        Task task = new Task();
        task.setId(cursor.getLong(COL_TASK_ID));
        task.setDescription(cursor.getString(COL_TASK_DESC));
        int isInBacklog = cursor.getInt(COL_TASK_IS_IN_BACKLOG);
        if (isInBacklog == TasksDbHelper.FALSE) {
            task.setInBacklog(false);
        } else {
            task.setInBacklog(true);
        }
        int isDone = cursor.getInt(COL_TASK_IS_DONE);
        if (isDone == TasksDbHelper.TRUE) {
            task.setDone(true);
        } else {
            task.setDone(false);
        }
        return task;
    }
}
