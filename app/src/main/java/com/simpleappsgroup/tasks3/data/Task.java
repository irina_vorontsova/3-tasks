package com.simpleappsgroup.tasks3.data;

public class Task {

    private long id;
    private String description;
    private boolean inBacklog = true;
    private boolean done = false;

    public Task() {
    }

    public Task(String description) {
        this.description = description;
    }

    public Task(String description, boolean inBacklog) {
        this.description = description;
        this.inBacklog = inBacklog;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isInBacklog() {
        return inBacklog;
    }

    public void setInBacklog(boolean inBacklog) {
        this.inBacklog = inBacklog;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
