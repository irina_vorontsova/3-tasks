package com.simpleappsgroup.tasks3.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

public class TasksContentProvider extends ContentProvider {

    public static final Uri TASKS_URI = Uri.withAppendedPath(TasksContract.BASE_CONTENT_URI, TasksContract.TaskEntry.TABLE_NAME);

    static final int TASKS_CODE = 100;

    private static final UriMatcher uriMatcher = buildUriMatcher();
    private TasksDbHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new TasksDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String table = getTableName(uri);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(table, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);

        switch (match) {
            case TASKS_CODE:
                return TasksContract.TaskEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table = getTableName(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        long value = database.insert(table, null, values);
        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.withAppendedPath(TasksContract.BASE_CONTENT_URI, String.valueOf(value));
    }

    @Override
    public int delete(Uri uri, String where, String[] args) {
        if (TextUtils.isEmpty(where)) {
            where = "1";
        }
        String table = getTableName(uri);
        SQLiteDatabase dataBase = dbHelper.getWritableDatabase();
        int deleted = dataBase.delete(table, where, args);
        if (deleted > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String whereClause, String[] whereArgs) {
        String table = getTableName(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        int rowsUpdated = database.update(table, values, whereClause, whereArgs);
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    public static String getTableName(Uri uri) {
        String value = uri.getPath();
        value = value.replace("/", "");//we need to remove '/'
        return value;
    }

    static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority = TasksContract.AUTHORITY;

        uriMatcher.addURI(authority, TasksContract.PATH_TASKS, TASKS_CODE);

        return uriMatcher;
    }
}
