package com.simpleappsgroup.tasks3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.simpleappsgroup.tasks3.data.TasksContract;
import com.simpleappsgroup.tasks3.data.TasksDbHelper;

import junit.framework.Assert;

import java.util.Map;
import java.util.Set;

public class TestUtils {

    public static ContentValues createTaskValues() {
        // Create a new map of values, where column names are the keys
        ContentValues testValues = new ContentValues();

        testValues.put(TasksContract.TaskEntry.TASK_DESC_COLUMN, "Finish homework");
        testValues.put(TasksContract.TaskEntry.TASK_DONE_COLUMN, 1);
        testValues.put(TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN, 1);

        return testValues;
    }

    public static long insertTaskValues(Context context) {
        TasksDbHelper dbHelper = new TasksDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues testValues = TestUtils.createTaskValues();

        long taskId;
        taskId = db.insert(TasksContract.TaskEntry.TABLE_NAME, null, testValues);

        return taskId;
    }

    public static void validateCursor(String error, Cursor valueCursor, ContentValues expectedValues) {
        Assert.assertTrue("Empty cursor returned. " + error, valueCursor.moveToFirst());
        validateCurrentRecord(error, valueCursor, expectedValues);
        valueCursor.close();
    }

    static void validateCurrentRecord(String error, Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            Assert.assertFalse("Column '" + columnName + "' not found. " + error, idx == -1);
            String expectedValue = entry.getValue().toString();
            Assert.assertEquals("Value '" + entry.getValue().toString() +
                    "' did not match the expected value '" +
                    expectedValue + "'. " + error, expectedValue, valueCursor.getString(idx));
        }
    }
}
