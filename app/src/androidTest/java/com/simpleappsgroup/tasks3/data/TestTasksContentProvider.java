package com.simpleappsgroup.tasks3.data;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.test.ProviderTestCase2;

import com.simpleappsgroup.tasks3.TestUtils;

import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.CONTENT_TYPE;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.CONTENT_URI;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry.TASK_IN_BACKLOG_COLUMN;
import static com.simpleappsgroup.tasks3.data.TasksContract.TaskEntry._ID;

public class TestTasksContentProvider extends ProviderTestCase2<TasksContentProvider> {

    private static final Uri INVALID_URI = Uri.withAppendedPath(TasksContract.BASE_CONTENT_URI, "invalid");

    private SQLiteDatabase testDatabase;

    public TestTasksContentProvider() {
        super(TasksContentProvider.class, TasksContract.AUTHORITY);
    }

    public TestTasksContentProvider(Class<TasksContentProvider> providerClass, String providerAuthority) {
        super(providerClass, providerAuthority);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    // the only test with real context
    public void testProviderRegistry() {
        PackageManager packageManager = mContext.getPackageManager();
        ComponentName componentName = new ComponentName(mContext.getPackageName(), TasksContentProvider.class.getName());

        try {
            ProviderInfo providerInfo = packageManager.getProviderInfo(componentName, 0);

            assertEquals("Error: TasksContentProvider registered with authority: " + providerInfo.authority +
                    " instead of authority: " + TasksContract.AUTHORITY, providerInfo.authority, TasksContract.AUTHORITY);
        } catch (PackageManager.NameNotFoundException e) {
            assertTrue("Error: TasksContentProvider not registered at " + mContext.getPackageName(), false);
        }
    }

    public void testGetType() {
        String type = getMockContentResolver().getType(CONTENT_URI);
        assertEquals("Error: the TaskEntry BASE_CONTENT_URI should return TaskEntry.CONTENT_TYPE",
                CONTENT_TYPE, type);
    }

    public void testGetTypeInvalidUri() {
        String type = getMockContentResolver().getType(INVALID_URI);
        assertEquals("No type should be returned for invalid uri from TasksContentProvider", null, type);
    }

    public void testsSimpleTasksQuery() {
        ContentValues taskValues = TestUtils.createTaskValues();
        insertTaskValues();

        Cursor tasksCursor = getMockContentResolver().query(CONTENT_URI, null, null, null, null);
        TestUtils.validateCursor("testsSimpleTasksQuery", tasksCursor, taskValues);
    }

    public void testTaskUpdate() {
        ContentValues taskValues = TestUtils.createTaskValues();
        long taskId = insertTaskValues();

        ContentValues updatedValues = new ContentValues(taskValues);
        updatedValues.put(_ID, taskId);
        updatedValues.put(TASK_IN_BACKLOG_COLUMN, 0);

        int count = getMockContentResolver().update(CONTENT_URI, updatedValues, _ID + "= ?", new String[]{Long.toString(taskId)});
        assertEquals(count, 1);

        Cursor cursor = getMockContentResolver().query(CONTENT_URI, null, _ID + " = " + taskId, null, null);
        TestUtils.validateCursor("testTaskUpdate: Error validating task update", cursor, updatedValues);
        cursor.close();
    }

    public void testDeleteTask() {
        long taskId = insertTaskValues();

        int deletedRows = getMockContentResolver().delete(CONTENT_URI, _ID + " = " + taskId, null);
        assertEquals(1, deletedRows);
    }

    public void testDeleteAllTask() {
        int numOfTasks = 4;
        for (int i = 0; i < numOfTasks; i++) {
            insertTaskValues();
        }

        int deletedRows = getMockContentResolver().delete(CONTENT_URI, null, null);
        assertEquals(numOfTasks, deletedRows);
    }

    private long insertTaskValues() {
        long taskId = TestUtils.insertTaskValues(getMockContext());
        assertTrue("Error: Failed to insert new task values", taskId != -1);
        return taskId;
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
